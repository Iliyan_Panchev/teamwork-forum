from fastapi import APIRouter, Response, Depends
from starlette.exceptions import HTTPException
from starlette.requests import Request
from core.authentication.jwt_bearer import JwtBearer
from core.authentication.jwt_handler import get_user_email
from core.errors import HTTPResponses
from core.models.topic import Topics , CreateTopicsModel
from routers.users import check_token
from services import topic_service, user_service, category_service , comment_service
from services.topic_service import check_for_author, is_admin, set_topic_lock
from services.utility import get_page, id_exists, paginate

topic_router = APIRouter(prefix='/topics', tags=['Forum Topics'], )


@topic_router.get('/', dependencies=[Depends(JwtBearer())])
def get_all_topics(
    request: Request,
    sort: str | None = None, 
    sort_by: str | None = None, 
    search: str | None = None,
    pagination: int | None = None,
    page: int | None = None
    ):
    user_id = user_service.get_user_by_mail(get_user_email(check_token(request)))
    if topic_service.is_admin(check_token(request)):
        result = topic_service.all_topics(search)
    else:
        result = topic_service.all_topics_without_private(user_id, search)
        
    if sort and (sort == 'asc' or sort == 'desc'):
        result = topic_service.sort(result, reverse=sort == 'desc', attribute=sort_by)
    if pagination:
        result = paginate(result, pagination)
    if page:
        result = get_page(result, page)    
    return result
 

@topic_router.get('/{id}', dependencies=[Depends(JwtBearer())])
def get_topic_by_id(
    id: int, 
    request: Request,
    sort_comments: str | None = None, 
    sort_comments_by: str | None = None, 
    search_comments: str | None = None,
    pagination: int | None = None,
    page: int | None = None    
    ):
    if not id_exists(id, 'topics'):
        raise HTTPException(status_code=404,detail=f'Comment with id: {id} does not exist.')
    user_id = user_service.get_user_by_mail(get_user_email(check_token(request)))
    current_cat = category_service.get_cat_id_from_forum_data(topic_id=id)
    user_access = category_service.check_category_for_access(current_cat, user_id)
    if category_service.is_private(current_cat) and user_access not in ('write_access', 'read_access'):
        raise HTTPException(status_code=403, detail='You need read or write access to update this private category topic.')
    topic = topic_service.get_by_id(id, search_comments)

    if sort_comments and (sort_comments == 'asc' or sort_comments == 'desc'):
        topic.comments = comment_service.sort_comment(topic.comments, reverse=sort_comments == 'desc', attribute=sort_comments_by)
    if pagination:
        topic.comments = paginate(topic.comments, pagination)
    if page:
        topic.comments = get_page(topic.comments, page)    
    return topic



@topic_router.post('/', dependencies=[Depends(JwtBearer())], status_code=201)
def create_topic(topic: CreateTopicsModel, request: Request):
    user_id = user_service.get_user_by_mail(get_user_email(check_token(request)))
    if topic_service.is_category_locked(topic.categories_id):
        raise HTTPException(status_code=403, detail='This topic is part of a locked Category')
    user_access = category_service.check_category_for_access(topic.categories_id, user_id)
    if category_service.is_private(topic.categories_id) and user_access not in ('write_access',):
        raise HTTPException(status_code=403, detail='You need write access to create this private category reply.')
    if topic_service.title_exists(topic.title, "topics"):
        raise HTTPException(status_code=400, detail=f'Topic with name {topic.title} already exist!')
 
    return topic_service.create(topic)


@topic_router.put('/{id}',dependencies=[Depends(JwtBearer())])
def update_topic(id: int, topic: Topics, request: Request):
    user_id = user_service.get_user_by_mail(get_user_email(check_token(request)))
    current_cat = category_service.get_cat_id_from_forum_data(topic_id=id)
    if topic_service.is_category_locked(current_cat):
        raise HTTPException(status_code=403, detail='This topic is part of a locked Category')
    user_access = category_service.check_category_for_access(current_cat, user_id)
    if category_service.is_private(current_cat) and user_access not in ('write_access',):
        raise HTTPException(status_code=403, detail='You need write access to update this private category reply.')
    existing_topic = topic_service.get_by_id(id)
    if existing_topic is None:
        return Response(status_code=404)
    return topic_service.update(existing_topic, topic)


@topic_router.patch('/{id}', dependencies=[Depends(JwtBearer())])
def set_best_comment(id: int, comment_id: int, request: Request):
    if not id_exists(comment_id,'comments'):
        raise HTTPException(status_code=404,detail=f'Comment with id {comment_id} does not exist.')
    if not id_exists(id,'topics'):
        raise HTTPException(status_code=404,detail=f'Topic with id {comment_id} does not exist.')
    if comment_service.is_topic_locked(id):
        raise HTTPException(status_code=400, detail='You can`t create comment because this topic is locked')
    x_token = check_token(request)
    topic: Topics = topic_service.get_by_id(id)
    if not check_for_author(x_token, topic.users_id):
        raise HTTPException(status_code=403, detail='You must be author on this topic')
    if topic_service.is_category_locked(topic.categories_id):
        raise HTTPException(status_code=400, detail='You cant set best comment because this category is locked')
    topic.best_comment_id = comment_id

    return topic


@topic_router.patch('/{id}/lock_topic', dependencies=[Depends(JwtBearer())])
def lock_topic(id: int, request: Request):
    if not is_admin(check_token(request)):
        raise HTTPException(status_code=403, detail='You must be admin to unlock this topic')
    topic: Topics = topic_service.get_by_id(id)
    result = set_topic_lock(id, True)
    topic.is_locked = True
    return f"Topic with title {topic.title} is locked"


@topic_router.patch('/{id}/unlock_topic', dependencies=[Depends(JwtBearer())])
def unlock_topic(id: int, request: Request):
    if not is_admin(check_token(request)):
        raise HTTPException(status_code=403, detail='You must be admin to unlock this topic')
    topic = topic_service.get_by_id(id)
    set_topic_lock(id, False)
    topic.is_locked = False
    return f"Topic with title {topic.title} is unlocked"


@topic_router.delete('/{topic_id}', dependencies=[Depends(JwtBearer())])
def delete_topic(id:int, request: Request):
    if not id_exists(id,'topics'):
        raise HTTPException(status_code=404,detail=f'Topic with id {id} does not exist.')    
    x_token = check_token(request)
    user_id = user_service.get_user_by_mail(get_user_email(x_token))
    if not check_for_author(x_token, user_id):
        raise HTTPException(status_code=403, detail='You must be author on this topic')

    return topic_service.delete(id)
