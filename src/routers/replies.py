from fastapi import APIRouter, Depends, Response
from starlette.exceptions import HTTPException
from starlette.requests import Request
from core.authentication.jwt_bearer import JwtBearer
from core.models.reply import Reply , CreateReplyModel
from routers.users import check_token
from services import reply_service, category_service, user_service, topic_service
from services.utility import id_exists
from core.authentication.jwt_handler import get_user_email

replies_router = APIRouter(prefix='/replies', tags=['Forum Replies'])


@replies_router.get('/', dependencies=[Depends(JwtBearer())])
def get_all_replies(request:Request):
    user_id = user_service.get_user_by_mail(get_user_email(check_token(request)))
    if topic_service.is_admin(check_token(request)):
        all_replies = reply_service.all()
    else:   
        all_replies = reply_service.all_without_private(user_id)
    return all_replies if all_replies else Response(status_code=404)


@replies_router.get('/{reply_id}',dependencies=[Depends(JwtBearer())])
def get_reply_by_id(reply_id: int, request: Request):
    if not id_exists(reply_id, 'replies'):
        raise HTTPException(status_code=404,detail=f'Reply with id: {reply_id} does not exist.')
    user_id = user_service.get_user_by_mail(get_user_email(check_token(request)))
    current_cat = category_service.get_cat_id_from_forum_data(reply_id=reply_id)
    user_access = category_service.check_category_for_access(current_cat, user_id)
    if category_service.is_private(current_cat) and user_access not in ('write_access', 'read_access'):
        raise HTTPException(status_code=403, detail='You need read or write access to view this private category reply.')
    return reply_service.get_by_id(reply_id)


@replies_router.post('/', dependencies=[Depends(JwtBearer())], status_code=201)
def create_reply(reply: CreateReplyModel, request: Request):
    user_id = user_service.get_user_by_mail(get_user_email(check_token(request)))
    current_cat = category_service.get_cat_id_from_forum_data(comment_id=reply.comment_id)
    if topic_service.is_category_locked(current_cat):
        raise HTTPException(status_code=403, detail='This reply is part of a locked Category')
    user_access = category_service.check_category_for_access(current_cat, user_id)
    if category_service.is_private(current_cat) and user_access not in ('write_access',):
        raise HTTPException(status_code=403, detail='You need write access to create this private category reply.')
    reply.users_id = user_id
    return reply_service.create(reply)


@replies_router.put('/{reply_id}', dependencies=[Depends(JwtBearer())])
def update_reply(reply_id: int, new_reply: Reply, request: Request):
    if not id_exists(reply_id,'replies'):
        raise HTTPException(status_code=404, detail=f'Reply with id: {reply_id} does not exist.')
    user_id = user_service.get_user_by_mail(get_user_email(check_token(request)))
    current_cat = category_service.get_cat_id_from_forum_data(reply_id=reply_id)
    if current_cat is None:
        raise HTTPException(status_code=404)
    user_access = category_service.check_category_for_access(current_cat, user_id)
    if category_service.is_private(current_cat) and user_access not in ('write_access',):
        raise HTTPException(status_code=403, detail='You need write access to update this private category reply.')
    old_reply = reply_service.get_by_id(reply_id)
    return reply_service.update(old_reply, new_reply)


@replies_router.delete('/{id}', dependencies=[Depends(JwtBearer())])
def delete_reply(id: int, request: Request):
    user_id = user_service.get_user_by_mail(get_user_email(check_token(request)))
    current_cat = category_service.get_cat_id_from_forum_data(reply_id=id)
    user_access = category_service.check_category_for_access(current_cat, user_id)
    if category_service.is_private(current_cat) and user_access not in ('write_access',):
        raise HTTPException(status_code=403, detail='You need write access to delete this private category reply.')
    reply_service.delete(id)

    return {
        'message':f'Reply with id:{id} deleted.'
    }


