from fastapi import APIRouter, Depends, HTTPException
from starlette.requests import Request
from starlette.responses import Response
from core.authentication.jwt_handler import get_user_email
from routers.users import check_token
from services.utility import users_id_exists ,id_exists
from core.authentication.jwt_bearer import JwtBearer
from core.errors import HTTPResponses
from core.models.comment import Comment , CreateCommentModel
from services import comment_service, topic_service, user_service, category_service

comment_router = APIRouter(prefix='/comments', tags=['Forum Comments'])


@comment_router.get('/', dependencies=[Depends(JwtBearer())])
def get_all_comments(request: Request):
    user_id = user_service.get_user_by_mail(get_user_email(check_token(request)))
    if topic_service.is_admin(check_token(request)):
        result = comment_service.all_comments()
    else:
        result = comment_service.all_comments_without_private(user_id)    
    if result is None:
        return HTTPResponses.NotFound('No comments.')

    return result


@comment_router.get('/{id}', dependencies=[Depends(JwtBearer())])
def get_by_id(id: int, request: Request):
    if not id_exists(id, 'comments'):
        raise HTTPException(status_code=404,detail=f'Comment with id: {id} does not exist.')
    user_id = user_service.get_user_by_mail(get_user_email(check_token(request)))
    current_cat = category_service.get_cat_id_from_forum_data(comment_id=id)
    user_access = category_service.check_category_for_access(current_cat, user_id)
    if category_service.is_private(current_cat) and user_access not in ('write_access', 'read_access'):
        raise HTTPException(status_code=403, detail='You need read or write access to view this private category comment.')
    comment = comment_service.get_by_id(id)
    return comment


@comment_router.post('/', dependencies=[Depends(JwtBearer())], status_code=201)
def create_comment(comment: CreateCommentModel, request: Request):
    if not id_exists(comment.topics_id, 'topics'):
        raise HTTPException(status_code=404,detail='You are trying to post a comment in non existent Topic.')
    user_id = user_service.get_user_by_mail(get_user_email(check_token(request)))
    current_cat = category_service.get_cat_id_from_forum_data(topic_id=comment.topics_id)
    if topic_service.is_category_locked(current_cat):
        raise HTTPException(status_code=403, detail='This comment is part of a locked Category')
    user_access = category_service.check_category_for_access(current_cat, user_id)
    if category_service.is_private(current_cat) and user_access not in ('write_access','read_access'):
        raise HTTPException(status_code=403, detail='You need write access to post comments in this category.')
    if comment_service.is_topic_locked(comment.topics_id):
        raise HTTPException(status_code=400, detail='You can`t create comment because this topic is locked')
    return comment_service.create(comment)


@comment_router.put('/{id}', dependencies=[Depends(JwtBearer())])
def update_comment(id: int, comment: Comment, request:Request):
    if not id_exists(id, 'comments'):
        raise HTTPException(status_code=404, detail=f'Comment with id {id} does not exist.')
    user_id = user_service.get_user_by_mail(get_user_email(check_token(request)))
    current_cat = category_service.get_cat_id_from_forum_data(comment_id=id)
    if topic_service.is_category_locked(current_cat):
        raise HTTPException(status_code=403, detail='This comment is part of a locked Category')
    user_access = category_service.check_category_for_access(current_cat, user_id)
    if category_service.is_private(current_cat) and user_access not in ('write_access',):
        raise HTTPException(status_code=403, detail='You need read or write access to view this private category comment.')
    existing_comment = comment_service.get_by_id(id)
    if existing_comment is None:
        return Response(status_code=404)
    if comment_service.is_topic_locked(existing_comment.topics_id):
        raise HTTPException(status_code=400, detail='You can`t update comment because this topic is locked')
    
    return comment_service.update(existing_comment, comment)
    
    
@comment_router.put('/{comments_id}/user/{users_id}', dependencies=[Depends(JwtBearer())], status_code=201)
def react_to_comment(comments_id, users_id, request: Request):
    user_id = user_service.get_user_by_mail(get_user_email(check_token(request)))
    current_cat = category_service.get_cat_id_from_forum_data(comment_id=comments_id)
    user_access = category_service.check_category_for_access(current_cat, user_id)
    if category_service.is_private(current_cat) and user_access not in ('write_access',):
        raise HTTPException(status_code=403, detail='You need write access to view this private category comment.')
    
    if not users_id_exists(users_id, 'comments_has_likes') \
        and not users_id_exists(users_id, 'comments_has_dislikes'):
        comment_service.like(comments_id, users_id)
        return {
            'message':f'User id: {users_id} liked comment id: {comments_id}.'
        }
    elif users_id_exists(users_id, 'comments_has_likes'):
        comment_service.remove_like(users_id)
        comment_service.dislike(comments_id, users_id)
        return {
            'message':f'User id: {users_id} disliked comment id: {comments_id}.'
        }
    elif users_id_exists(users_id, 'comments_has_dislikes'):
        comment_service.remove_dislike(users_id)
        return {
            'message':f'User id: {users_id} neither likes or dislikes comment id: {comments_id}.'
        }


@comment_router.delete('/{comment_id}', status_code=404, dependencies=[Depends(JwtBearer())])
def delete_comment(comment_id, request: Request):
    user_id = user_service.get_user_by_mail(get_user_email(check_token(request)))
    current_cat = category_service.get_cat_id_from_forum_data(comment_id=comment_id)
    user_access = category_service.check_category_for_access(current_cat, user_id)
    if category_service.is_private(current_cat) and user_access not in ('write_access',):
        raise HTTPException(status_code=403, detail='You need write access to view this private category comment.')
    if not id_exists(comment_id, 'comments'):
        raise HTTPException(status_code=404, detail=f'Comment with id: {comment_id} does not exist.')
    comment_service.delete(comment_id)

    return {
        'message':f'Comment with id: {comment_id} deleted.'
    }

 
