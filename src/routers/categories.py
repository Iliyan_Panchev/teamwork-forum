from fastapi import APIRouter, Depends, Request, Response
from starlette.exceptions import HTTPException
from core.authentication.jwt_bearer import JwtBearer
from core.authentication.jwt_handler import get_user_email
from core.models.category import Category, CreateCategoryModel
from routers.users import check_token
from services import category_service, topic_service , user_service
from services.category_service import set_category_lock
from services.topic_service import is_admin
from services.utility import id_exists, paginate , get_page

categories_router = APIRouter(prefix='/categories', tags=['Forum Categories'])



@categories_router.get('/', dependencies=[Depends(JwtBearer())])
def get_all_categories(
    request:Request,
    sort: str | None = None,
    sort_by: str | None = None,
    search: str | None = None,
    pagination: int | None = None,
    page: int | None = None
):   
    user_id = user_service.get_user_by_mail(get_user_email(check_token(request)))
    if topic_service.is_admin(check_token(request)):
        all_categories = category_service.all_cat(search)
    else:
        all_categories = category_service.all_categories_without_private(user_id, search)

    if sort and (sort == 'asc' or sort == 'desc'):
       all_categories = category_service.sort_cat(all_categories, reverse=sort == 'desc', attribute=sort_by)
    if pagination:
       all_categories = paginate(all_categories, pagination)
    if page:
        all_categories = get_page(all_categories, page)

    return all_categories if all_categories else {
        'message':'No categories yet.'
    }


@categories_router.get('/{id}', dependencies=[Depends(JwtBearer())])
def get_category_by_id(
    id: int,
    request: Request,
    sort_topics: str | None = None,
    sort_topics_by: str | None = None,
    search_topics: str | None = None,
    pagination: int | None = None,
    page: int | None = None,
    ):
    if not id_exists(id, 'categories'):
        raise HTTPException(status_code=404, detail=f'Category with id: {id} does not exist.')
    user_id = user_service.get_user_by_mail(get_user_email(check_token(request)))
    user_access = category_service.check_category_for_access(id, user_id)
    if category_service.is_private(id) and user_access not in ('write_access', 'read_access'):
        raise HTTPException(status_code=403, detail='You need read or write access to view this private category.')
    
    category = category_service.get_by_id(id, search_topics)
  
    if sort_topics and (sort_topics == 'asc' or sort_topics == 'desc'):
       category = topic_service.sort(category.topics, reverse=sort_topics == 'desc', attribute=sort_topics_by)
    if pagination:
        category.topics = paginate(category.topics, pagination)
    if page:
        category.topics = get_page(category.topics, page)    

    return category if category else Response(status_code=404)


@categories_router.post('/', dependencies=[Depends(JwtBearer())], status_code=201)
def create_category(category: CreateCategoryModel, request: Request):
    if not is_admin(check_token(request)):
        raise HTTPException(status_code=403, detail='Only admins can create categories.')
    if category_service.name_exists(category.name, 'categories'):
        raise HTTPException(status_code=400, detail=f'Category with name: {category.name} already exist!')
    return category_service.create_cat(category)


@categories_router.delete('/{id}', dependencies=[Depends(JwtBearer())])
def delete_category(id: int, request: Request):
    if not id_exists(id, 'categories'):
        raise HTTPException(status_code=404, detail=f'Category with id: {id} does not exist')   
    if not is_admin(check_token(request)):
        raise HTTPException(status_code=403, detail='Only admins can delete categories.')
    category_service.delete_cat(id)

    return{
        'message':f'Category with id: {id} deleted.'
        }


@categories_router.patch('/{id}/lock_category', dependencies=[Depends(JwtBearer())])
def lock_category(id: int, request: Request):
    if not id_exists(id, 'categories'):
        raise HTTPException(status_code=404, detail=f'Category with id: {id} does not exist') 
    if not is_admin(check_token(request)):
        raise HTTPException(status_code=403, detail='You must be admin to lock this category')
    category = category_service.get_by_id(id)
    set_category_lock(id, True)
    category.is_locked = True
    return {
        'message':f"Category with id {id} is locked"
    }


@categories_router.patch('/{id}/unlock_category', dependencies=[Depends(JwtBearer())])
def unlock_category(id: int, request: Request):
    if not id_exists(id, 'categories'):
        raise HTTPException(status_code=404, detail=f'Category with id: {id} does not exist') 
    if not is_admin(check_token(request)):
        raise HTTPException(status_code=403, detail='You must be admin to unlock this category')
    category = category_service.get_by_id(id)
    set_category_lock(id, False)
    category.is_locked = False

    return {
        'message':f"Category with id {id} is unlocked"
    }


@categories_router.put('/{id}', dependencies=[Depends(JwtBearer())])
def edit_category(id: int, new_category: Category, request: Request):
    if not id_exists(id, 'categories'):
        raise HTTPException(status_code=404, detail=f'Category with id: {id} does not exist') 
    if not is_admin(check_token(request)):
        raise HTTPException(status_code=403, detail='You must be admin to edit this category.')
    existing_category = category_service.get_by_id(id)
    if existing_category is None:
        raise HTTPException(status_code=404)
    return category_service.update_cat(existing_category, new_category)


@categories_router.patch('/{id}/private', dependencies=[Depends(JwtBearer())])
def make_category_private(id: int, request: Request):
    if not id_exists(id, 'categories'):
        raise HTTPException(status_code=404, detail=f'Category with id: {id} does not exist') 
    if not is_admin(check_token(request)):
        raise HTTPException(status_code=403, detail='You must be admin to make category private.')
    category = category_service.get_by_id(id)
    category_service.set_private(id, True)
    category.is_private = True
    return {
        'message':f"Category with name {category.name} is private now."
    }


@categories_router.patch('{id}/non_private', dependencies=[Depends(JwtBearer())])
def make_category_non_private(id: int, request: Request):
    if not id_exists(id, 'categories'):
        raise HTTPException(status_code=404, detail=f'Category with id: {id} does not exist') 
    if not is_admin(check_token(request)):
        raise HTTPException(status_code=403, detail='You must be admin to make category non private.')
    category = category_service.get_by_id(id)
    category_service.set_non_private(id, False)
    category.is_private = False
    return {
        'message':f"Category with name {category.name} is non private now."
    }


@categories_router.post('/{category_id}/users/{user_id}/read_access', dependencies=[Depends(JwtBearer())])
def give_read_access(category_id: int, user_id: int, request: Request):
    if not id_exists(category_id, 'categories'):
        raise HTTPException(status_code=404, detail=f'Category with id: {id} does not exist') 
    if not is_admin(check_token(request)):
        raise HTTPException(status_code=403, detail='You must be admin to give access.')
    return category_service.read_access(category_id, user_id)


@categories_router.post('/{category_id}/users/{user_id}/write_access', dependencies=[Depends(JwtBearer())])
def give_write_access(category_id: int, user_id: int, request: Request):
    if not id_exists(category_id, 'categories'):
        raise HTTPException(status_code=404, detail=f'Category with id: {id} does not exist') 
    if not is_admin(check_token(request)):
        raise HTTPException(status_code=403, detail='You must be admin to give access.')
    return category_service.write_access(category_id, user_id)


@categories_router.delete('/{category_id}/user/{user_id}/revoke_access', dependencies=[Depends(JwtBearer())])
def revoke_user_access(category_id: int, user_id: int, request: Request):
    if not id_exists(category_id, 'categories'):
        raise HTTPException(status_code=404, detail=f'Category with id: {id} does not exist') 
    if not is_admin(check_token(request)):
        raise HTTPException(status_code=403, detail='You must be admin to revoke access.')
    category_service.revoke_access(category_id, user_id)

    return {
        'message':f'User {user_id} lost his access to category {category_id}'
    }


@categories_router.get('/private/{id}/users_privileged', dependencies=[Depends(JwtBearer())])
def view_privileged(id: int, request: Request):
    if not id_exists(id, 'categories'):
        raise HTTPException(status_code=404, detail=f'Category with id: {id} does not exist') 
    if not is_admin(check_token(request)):
        raise HTTPException(status_code=403, detail='You must be admin to view priviliged.')
    return category_service.get_privileged_users(id)



