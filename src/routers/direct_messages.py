from fastapi import APIRouter, Depends, Request , HTTPException
from core.authentication.jwt_bearer import JwtBearer
from core.models.direct_message import DirectMessage, DirectMessageDisplay, CreateDirectMessageModel
from routers.users import check_token
from services import dm_service
from services.utility import id_exists

messages_router = APIRouter(prefix='/messages', tags=['Forum Chats'])


@messages_router.get('/', dependencies=[Depends(JwtBearer())])
def display_all_sent_messages(request:Request):
    check_token(request)
    return dm_service.all()


@messages_router.get('/{message_id}')
def get_message_by_id(message_id):
    if not id_exists(message_id, 'direct_messages'):
        raise HTTPException(status_code=404,detail=f'Message with id:{message_id} does not exist')
    return dm_service.get_by_id(message_id)


@messages_router.get('/user/{user_id}', dependencies=[Depends(JwtBearer())])
def get_users_messages(user_id, request: Request):
    check_token(request)
    if not dm_service.id_exists(user_id, 'users'):
        raise HTTPException(status_code=404, detail=f'User with id:{user_id} does not exist.')
    
    return dm_service.get_by_receiver_id(user_id)


@messages_router.get('/conversation/user/{sender_id}/to/{receiver_id}', dependencies=[Depends(JwtBearer())])
def get_convo(sender_id: int, receiver_id: int, request: Request):
    check_token(request)
    if not dm_service.id_exists(sender_id, 'users'):
        raise HTTPException(status_code=404, detail=f'Sender with id:{sender_id} does not exist.')
    if not dm_service.id_exists(receiver_id, 'users'):
        raise HTTPException(status_code=404, detail=f'Receiver with id:{receiver_id} does not exist.')

    return sorted(dm_service.get_by_receiver_id(receiver_id) + \
                  dm_service.get_by_receiver_id(sender_id), key=lambda obj: obj.sent_at)


@messages_router.put('/edit/{message_id}', dependencies=[Depends(JwtBearer())])
def edit_message(new_message: DirectMessage, message_id: int, request: Request):
    check_token(request)
    if not dm_service.id_exists(message_id,'direct_messages'):
        raise HTTPException(status_code=404,detail=f'Message with id:{message_id} does not exist.')
    old_message = dm_service.get_by_id(message_id)
    return dm_service.update(old_message, new_message)


@messages_router.post('/user/{sender_id}/send/user/{receiver_id}', dependencies=[Depends(JwtBearer())], status_code=201)
def create_new_convo(direct_message: CreateDirectMessageModel, sender_id: int, receiver_id: int, request: Request):
    check_token(request)
    if sender_id == receiver_id:
        raise HTTPException(status_code=400, detail='You can not message yourself.')
    if not dm_service.id_exists(sender_id, 'users'):
        raise HTTPException(status_code=404, detail=f'User with id:{sender_id} does not exist.')
    if not dm_service.id_exists(receiver_id, 'users'):
        raise HTTPException(status_code=404, detail=f'User with id:{receiver_id} does not exist.')
    
    dm_service.send_message(direct_message, sender_id, receiver_id)
    
    return {
        'msg':'message sent'
        }

@messages_router.delete('/user/{receiver_id}', dependencies=[Depends(JwtBearer())])
def delete_convo(receiver_id: int, request: Request):
    check_token(request)
    if not id_exists(receiver_id, 'users'):
        raise HTTPException(status_code=404, detail=f'Receiver with id:{receiver_id} does not exist.')
    dm_service.delete(receiver_id)
  


@messages_router.get('/user/{user_id}/contacts', dependencies=[Depends(JwtBearer())])
def get_contancts_ids(user_id, request:Request):
    check_token(request)
    if not dm_service.id_exists(user_id, 'users'):
        raise HTTPException(status_code=404, detail=f'User with id: {user_id} does not exist.')
    contancts = dm_service.get_contacts(user_id)
    return {
        'contacts':(c[0] for c in contancts)
        }
 





    
