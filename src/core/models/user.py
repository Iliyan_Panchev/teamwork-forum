from datetime import datetime

from pydantic import BaseModel, EmailStr, constr


class User(BaseModel):
    id: int | None
    name: constr(min_length=3, max_length=15)
    email: EmailStr
    password: str
    create_at: datetime | None
    update_at: datetime | None
    admin: int = 0

    @classmethod
    def from_query_result(cls, id, name, email, password, create_at, update_at, admin):
        return cls(
            id=id,
            name=name,
            email=email,
            password=password,
            create_at=create_at,
            update_at=update_at,
            admin=admin)


class UserLogin(BaseModel):
    email: EmailStr
    password: str

class CreateUserModel(BaseModel):
    id: int | None
    name: constr(min_length=3, max_length=15)
    create_at: datetime | None
    email: EmailStr
    password: str
