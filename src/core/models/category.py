from datetime import date, datetime
from pydantic import BaseModel,  constr
from core.models.topic import Topics


class Category(BaseModel):
    id: int | None
    name: constr(min_length=3, max_length=50)
    created: datetime | None
    updated: datetime | None
    is_locked: bool | None = 0
    is_private: bool | None = 0
    topics: list[Topics] = []

    @classmethod
    def from_query_result(cls, id, name, created, updated, is_locked, is_private):
        return cls(
            id=id,
            name=name,
            created=created,
            updated=updated,
            is_locked=is_locked,
            is_private=is_private
            )
    
    

class CreateCategoryModel(BaseModel):
    id: int | None
    name: constr(min_length=3, max_length=50)
    created: datetime | None

    

        