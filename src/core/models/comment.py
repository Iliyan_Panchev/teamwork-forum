from datetime import datetime

from pydantic import BaseModel , constr

from core.models.reply import Reply
from core.models.user import User


class Comment(BaseModel):
    id: int | None
    body: constr(min_length=10,max_length=250)
    create_at: datetime | None
    update_at: datetime | None
    topics_id: int
    users_id: int
    likes: list[User] = []
    dislikes: list[User] = []
    replies: list[Reply] = []
    
    @classmethod
    def from_query_result(cls, id, body, create_at, update_at, topics_id, users_id):
        return cls(
            id=id,
            body=body,
            create_at=create_at,
            update_at=update_at,
            topics_id=topics_id,
            users_id=users_id)

class CreateCommentModel(BaseModel):
    id: int | None
    body: constr(min_length=10,max_length=250)
    create_at: datetime | None
    topics_id: int
    users_id: int


   
