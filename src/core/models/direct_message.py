from datetime import datetime
from pydantic import BaseModel , constr


class DirectMessage(BaseModel):
    id: int | None
    body: constr(min_length=10, max_length=150)
    sent_at: datetime | None
    sender: int 
    receiver: int 


class DirectMessageDisplay(BaseModel):
    id: int
    body: str
    sent_at: datetime 
    sender: int | None
    receiver: int | None


    @classmethod
    def from_query_result(cls, id, body, sent_at, sender, receiver):
        return cls(
                   id=id,
                   body=body,
                   sent_at=sent_at,
                   sender=sender,
                   receiver=receiver
                   )

class CreateDirectMessageModel(BaseModel):
    id: int | None
    body: constr(min_length=10, max_length=150)
    sent_at: datetime | None
