from datetime import datetime
from pydantic import BaseModel
from pydantic.types import constr
from core.models.comment import Comment


class Topics(BaseModel):
    id: int | None
    title: constr(min_length=3, max_length=150)
    body: constr(min_length=5, max_length=300)
    create_at: datetime | None
    update_at: datetime | None
    categories_id: int | str
    users_id: int | str 
    best_comment_id: int | None
    is_locked: bool | None = 0
    comments: list[Comment] = []
    

    @classmethod
    def from_query_result(cls, id, title, body, create_at, update_at, categories_id, users_id, best_comment_id, is_locked):
        return cls(
            id=id,
            title=title,
            body=body,
            create_at=create_at,
            update_at=update_at,
            categories_id=categories_id,
            users_id=users_id,
            best_comment_id=best_comment_id,
            is_locked=is_locked)

    @classmethod
    def with_comments(cls, id, title, body, create_at, update_at, categories_id, users_id, best_comment_id, is_locked, comments):
        return cls(
            id=id,
            title=title,
            body=body,
            create_at=create_at,
            update_at=update_at,
            categories_id=categories_id,
            users_id=users_id,
            best_comment_id=best_comment_id,
            is_locked=is_locked,
            comments=comments)


class CreateTopicsModel(BaseModel):
    id: int | None
    title: constr(min_length=3, max_length=150)
    body: constr(min_length=5, max_length=300)
    create_at: datetime | None
    categories_id: int
    users_id: int