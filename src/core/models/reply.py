from datetime import datetime
from pydantic import BaseModel, constr


class Reply(BaseModel):
    id: int | None
    body: constr(min_length=10,max_length=200)
    created: datetime | None
    updated: datetime | None
    comment_id: int
    users_id: int

    @classmethod
    def from_query_result(cls, id, body, created, updated, comment_id, users_id):
        return cls(
            id=id,
            body=body,
            created=created,
            updated=updated,
            comment_id=comment_id,
            users_id=users_id)

class CreateReplyModel(BaseModel):
    id: int | None
    body: constr(min_length=10,max_length=200)
    created: datetime | None
    comment_id: int
    users_id: int | None