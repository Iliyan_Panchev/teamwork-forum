from datetime import datetime
from fastapi import Response
from database.database_connection import read_query, insert_query, update_query, read_query_one
from core.models.direct_message import DirectMessage, DirectMessageDisplay
from services.utility import CURRENT_DATE_TIME, id_exists, name_exists
from services.user_service import get_by_id

def all():
    messages = read_query('''SELECT * FROM direct_messages''')
    
    return (DirectMessageDisplay.from_query_result(*m) for m in messages)\
          if messages else Response(status_code=404)

def get_by_id(id: int):
    message = read_query_one('''
    SELECT * FROM direct_messages WHERE id = ?''', (id,))
    
    return DirectMessageDisplay.from_query_result(*message) \
        if message else Response(status_code=404)

def get_by_receiver_id(id: int):
    messages = read_query('''
    SELECT * FROM direct_messages WHERE receiver_id = ?''', (id,))
    
    return [DirectMessageDisplay.from_query_result(*m) for m in messages] 


def send_message(direct_message: DirectMessage, sender_id: int, receiver_id: int):
    generated_id = insert_query(
                 'INSERT INTO direct_messages(body, sent_at, sender_id, receiver_id) VALUES (?, ?, ?, ?)',
                 (direct_message.body, CURRENT_DATE_TIME, sender_id, receiver_id)
                 )
    direct_message.id = generated_id
    direct_message.sent_at = CURRENT_DATE_TIME
    return direct_message



def update(old_message: DirectMessage, new_message: DirectMessage):
    merged = DirectMessage(
        id=old_message.id,
        body=new_message.body or old_message.body,
        sent_at=old_message.sent_at,
        sender=old_message.sender,
        receiver=old_message.receiver,
        
    )

    update_query(
        '''UPDATE direct_messages SET
           body = ?
           WHERE id = ? 
        ''',
        (merged.body, merged.id))

    return merged


def delete(receiver_id: int):
    insert_query('DELETE FROM direct_messages WHERE receiver_id = ?',
                 (receiver_id,))


def get_contacts(user_id: int):
    data = read_query('SELECT DISTINCT receiver_id FROM direct_messages WHERE sender_id = ?',
               (user_id,))
    return data