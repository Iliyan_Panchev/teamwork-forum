from fastapi import Response
from database.database_connection import read_query, insert_query, update_query, read_query_one
from core.models.reply import Reply , CreateReplyModel
from services.utility import CURRENT_DATE_TIME

def all():
    replies = read_query('SELECT * FROM replies')
     
    return (Reply.from_query_result(*reply) for reply in replies) if replies else Response(status_code=404)


def all_without_private(user_id: int):
    replies = read_query('''SELECT replies.id, replies.body, replies.create_at, replies.update_at ,replies.comment_id, replies.users_id FROM categories
    JOIN topics ON topics.categories_id = categories.id 
    JOIN comments ON comments.topics_id = topics.id
    JOIN replies ON replies.comment_id = comments.id
    WHERE categories.is_private != 1 OR categories.id IN (SELECT categories_id FROM private_categories WHERE users_id = ?)''', 
    (user_id,))

    return (Reply.from_query_result(*reply) for reply in replies) if replies else Response(status_code=404)


def get_by_id(id: int):
    reply = read_query_one('SELECT * FROM replies WHERE id = ?',
                              (id,))    
    return Reply.from_query_result(*reply) if reply else Response(status_code=404)


def create(reply: Reply):
    reply_id = insert_query(
                 'INSERT INTO replies (body, create_at, comment_id, users_id) VALUES (?, ?, ?, ?)',
                 (reply.body, CURRENT_DATE_TIME, reply.comment_id, reply.users_id)
                 )
    reply.id = reply_id
    reply.created = CURRENT_DATE_TIME

    return reply


def update(old_reply: Reply, new_reply: Reply):
    merged = Reply(
        id=old_reply.id,
        body=new_reply.body or old_reply.body,
        created=old_reply.created,
        updated=CURRENT_DATE_TIME,
        comment_id=old_reply.comment_id,
        users_id=old_reply.users_id
    )

    update_query(
        '''UPDATE replies SET
           body = ?, update_at = ?
           WHERE id = ? 
        ''',
        (merged.body, merged.updated, merged.id))
    
    return merged


def delete(id: int):
    insert_query('DELETE FROM replies WHERE id = ?',
                 (id,))
    

def exists(id: int, table_name) -> bool:
    return any(
        read_query(
            f'SELECT id FROM {table_name} where id = ?',
            (id,)))


