from datetime import datetime

from core.models.user import User, UserLogin
from database.database_connection import read_query, insert_query, update_query, read_query_one
from services.utility import CURRENT_DATE_TIME , create_hash 


def all_users():
    data = read_query(
        '''SELECT id, name, email, password, create_at, update_at, admin
        from users''')
    if data is None:
        return None

    return (User.from_query_result(*row) for row in data)


def get_by_id(id: int):
    data = read_query(
        '''SELECT id, name, email, password, create_at, update_at, admin
        from users
        where id = ?''', (id,))
    return next((User.from_query_result(*row) for row in data), None)


def create(user: User, admin: bool=False):
    hashed_password = create_hash(user.password)

    generated_id = insert_query(
        'INSERT INTO users(name, email, password, create_at, admin) VALUES(?,?,?,?,?)',
        (user.name, user.email, hashed_password, CURRENT_DATE_TIME, admin))

    user.id = generated_id
    user.create_at = CURRENT_DATE_TIME

    return user


def update(old: User, new: User):
    merged = User(
        id=old.id,
        name=new.name or old.name,
        email=new.email or old.email,
        password=new.password or old.password,
        create_at=old.create_at,
        update_at=CURRENT_DATE_TIME,
        admin=new.admin or old.admin)

    update_query(
        '''UPDATE users SET
           name = ?, email = ?, password = ? ,create_at = ?, update_at = ?, admin = ?
           WHERE id = ? 
        ''',
        (merged.name, merged.email, merged.password, merged.create_at, merged.update_at, merged.admin, merged.id))

    return merged


def delete(id: int):
    insert_query('DELETE FROM users WHERE id = ?',
                 (id,))


def check_user(data: UserLogin):
    user = read_query('''Select email, password from users where email = ?''', (data.email, ))
    hashes_password = create_hash(data.password)
    if user[0][0] == data.email and user[0][1] == hashes_password:
        return True
    return False


def get_user_by_mail(email:str):
    id = read_query_one('SELECT id FROM users WHERE email=?',
                        (email,))
    
    return id[0] if id else None