from fastapi import HTTPException, Response
from core.authentication.jwt_handler import decodeJWT
from core.models.comment import Comment
from core.models.topic import Topics
from database.database_connection import read_query, insert_query, update_query, read_query_one
from services import category_service, user_service
from services.utility import CURRENT_DATE_TIME, id_exists



def all_topics(search: str = None):
    if search is None:
        data = read_query(
            '''SELECT *
               FROM topics''')
    else:
        data = read_query(
            '''SELECT *
               FROM topics 
               WHERE title LIKE ?''', (f'%{search}%',))

    result = (Topics.from_query_result(*row) for row in data)

    return result if result else Response(status_code=404)


def all_topics_without_private(user_id: int, search: str = None):
    if search is None:
        data = read_query('''
        SELECT T.id, T.title, T.body, T.create_at, T.update_at, T.categories_id, T.users_id, T.best_comment_id, T.is_locked FROM categories AS CT
        JOIN topics AS T ON T.categories_id = CT.id 
        WHERE CT.is_private != 1 OR CT.id IN (SELECT categories_id FROM private_categories WHERE users_id = ?)''', 
        (user_id,))
    else:
        data = read_query('''
        SELECT T.id, T.title, T.body, T.create_at, T.update_at, T.categories_id, T.users_id, T.best_comment_id, T.is_locked FROM categories AS CT
        JOIN topics AS T ON T.categories_id = CT.id 
        WHERE CT.is_private != 1 OR CT.id IN (SELECT categories_id FROM private_categories WHERE users_id = ?) AND title LIKE ?''', 
        (user_id, f'%{search}%' ))

    return (Topics.from_query_result(*row) for row in data) if data else Response(status_code=404)


def get_by_id(id, search_comments: str=None):
    data = read_query(
        '''SELECT *
            FROM topics
            WHERE id = ?''', (id,))
    if not any(data):
        raise HTTPException(status_code=404)
    topic = next((Topics.from_query_result(*row, ) for row in data), None)
    if search_comments:
        topic_comments = read_query('SELECT * FROM comments WHERE topics_id=? AND body LIKE ?',(id, f'%{search_comments}%'))
    else:
        topic_comments = read_query('''SELECT * FROM comments where topics_id = ?''', (id,))
    comments = [Comment.from_query_result(*x) for x in topic_comments]
    topic.comments = sorted(comments, key=lambda c: c.create_at)
    return topic


def sort(topics: list[Topics], *, attribute='created', reverse=False):
    if attribute == 'created':
        def sort_fn(t: Topics): return t.create_at
    elif attribute == 'updated':
        def sort_fn(t: Topics): return t.update_at
    elif attribute == 'title':
        def sort_fn(t: Topics): return t.title
    else:
        def sort_fn(t: Topics): return t.id

    return sorted(topics, key=sort_fn, reverse=reverse)


def create(topic: Topics):
    generated_id = insert_query(
        'INSERT INTO topics(title, body, create_at, categories_id, users_id, best_comment_id, is_locked) '
        'VALUES(?,?,?,?,?,?,?)',
        (topic.title, topic.body, CURRENT_DATE_TIME, topic.categories_id, topic.users_id, None, False))

    topic.id = generated_id
    topic.create_at = CURRENT_DATE_TIME
    topic.categories_id = category_service.get_by_id(topic.categories_id).name
    return topic


def title_exists(title: str, table_name: str) -> bool:
    return any(
        read_query(
            f'SELECT title FROM {table_name} where title = ?',
            (title,)))


def exists(id: int):
    return any(
        read_query(
            'select id, title from topics where id = ?',
            (id,)))


def update(old: Topics, new: Topics):
    merged = Topics(
        id=old.id,
        title=new.title or old.title,
        body=new.body or old.body,
        create_at=old.create_at,
        update_at=CURRENT_DATE_TIME,
        categories_id=new.categories_id or old.categories_id,
        users_id=new.users_id,
        best_comment_id=new.best_comment_id or old.best_comment_id)

    update_query(
        '''UPDATE topics SET
           title = ?, body = ?, update_at = ?, categories_id = ?
           WHERE id = ? 
        ''',
        (merged.title, merged.body, merged.update_at, merged.categories_id,
         merged.id))
    merged.create_at = old.create_at
    merged.users_id = old.users_id
    return merged


def delete(topic_id: int):
    insert_query('DELETE FROM topics WHERE id = ?',(topic_id,))
    return {'message':f'Topic with id {topic_id} deleted.'}


def check_for_author(token: str, topic_author_id: int):
    payload = decodeJWT(token)
    email = payload['userID']

    user_id = read_query('''
    select id from users where email = ?''', (email,))
    real_user_id = user_id[0][0]
    print(real_user_id)
    if real_user_id == topic_author_id:
        return True
    return False


def is_admin(token: str):
    payload = decodeJWT(token)
    email = payload['userID']
    user_from_database = read_query('''
    SELECT admin FROM users WHERE email = ?''', (email,))
    admin = user_from_database[0][0]
    return admin


def set_topic_lock(id: int, lock: bool):
    update_query('''
                update topics set is_locked = ? where id = ?''', (lock, id))
    if lock is True:
        return "Topic is locked"
    else:
        return "Topic is unlocked"


def is_category_locked(category_id: int):
    category_lock = read_query_one('SELECT is_locked FROM categories WHERE id = ?', (category_id,))
    return bool(category_lock[0])
