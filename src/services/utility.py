import datetime
import hashlib

from fastapi import Response
from database.database_connection import read_query
from core.models.category import Category

CURRENT_DATE_TIME =  datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
HASHING_ALGORITHM = 'SHA256'

def name_exists(name: str, table_name: str) -> bool:
    return any(
        read_query(
            f'SELECT name FROM {table_name} where name = ?',
            (name,)))


def id_exists(id: int, table_name: str) -> bool:
    return any(
        read_query(
            f'SELECT id FROM {table_name} where id = ?',
            (id,)))

def email_exists(email: str, table_name: str) -> bool:
    return any(
        read_query(
            f'SELECT email FROM {table_name} where email = ?',
            (email,)))

def users_id_exists(users_id: int, table_name: str) -> bool:
    return any(
        read_query(
            f'SELECT users_id FROM {table_name} where users_id = ?',
            (users_id,)))

def comments_id_exists(comments_id: int, table_name: str) -> bool:
    return any(
        read_query(
            f'SELECT comments_id FROM {table_name} where comments_id = ?',
            (comments_id,)))


def create_hash(data: str, algo=HASHING_ALGORITHM) -> str:
    byte_string = data.encode('utf-8')
    current_hash = hashlib.new(algo, byte_string)

    return current_hash.hexdigest()


def paginate(data: list, separate: int=0):
    if separate == 0:
        return data
    result = []
    counter = 1
    for index, value in enumerate(data):
        next = (separate * counter)
        if index == next:
            result.append({'Page': counter
            })
            next += separate
            counter += 1
        result.append(value) 
    result.append({'Page': counter})       
    return result        

 
def get_page(data: list[dict], page: int=None):
    result = []
    start_at = False
    for d in data:
        if page == 1:
            result.append(d)
            if isinstance(d, dict) and d['Page'] == page:  
                break  
        if isinstance(d, dict) and d['Page'] == page - 1:
            start_at = True
            continue
        if start_at:
            result.append(d)
        if isinstance(d, dict) and d['Page'] == page:  
            break  
        
    return result if result else Response(
        status_code=404, content='Page doesn not exist.'
        )

