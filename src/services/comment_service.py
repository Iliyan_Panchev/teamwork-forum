from datetime import datetime

from fastapi import Response, HTTPException
from services.utility import comments_id_exists
from core.models.comment import Comment , CreateCommentModel
from core.models.reply import Reply
from database.database_connection import read_query, insert_query, update_query , read_query_one
from services import topic_service
from services.utility import CURRENT_DATE_TIME


def all_comments():
    data = read_query(
            '''SELECT * FROM comments''')
    result = (Comment.from_query_result(*row) for row in data)
    return result if result else Response(status_code=404)


def all_comments_without_private(user_id: int):
    data = read_query('''SELECT comments.id, comments.body, comments.create_at, comments.update_at, comments.topics_id, comments.users_id FROM categories
    JOIN topics ON topics.categories_id = categories.id 
    JOIN comments ON comments.topics_id = topics.id
    WHERE categories.is_private != 1 OR categories.id IN (SELECT categories_id FROM private_categories WHERE users_id = ?)''',
    (user_id,))
    return (Comment.from_query_result(*row) for row in data) if data else Response(status_code=404)


def sort_comment(categories: list[Comment], *, attribute='id', reverse=False):
    if attribute == 'create_at':
        def sort_fn(c: Comment): return c.create_at
    elif attribute == 'users_id':
        def sort_fn(c: Comment): return c.users_id    
    elif attribute == 'update_at':
        def sort_fn(c: Comment): return c.update_at
    else:
        def sort_fn(c: Comment): return c.id

    return sorted(categories, key=sort_fn, reverse=reverse)


def get_by_id(id: int):
    data = read_query(
        '''SELECT *
            FROM comments
            WHERE id = ?''', (id,))

    comment = next((Comment.from_query_result(*row) for row in data), None)
    if not comment:
        raise HTTPException(status_code=404)
    comment_replies = read_query('''SELECT * FROM replies where comment_id = ?''', (id,))
    replies = [Reply.from_query_result(*x) for x in comment_replies]
    comment.replies = replies
    if comments_id_exists(id, 'comments_has_likes'):
        users_likes = read_query('''SELECT U.name FROM users AS U
                                INNER JOIN comments_has_likes AS CHD ON CHD.users_id = U.id
                                WHERE CHD.comments_id = ?''', (id,))
        comment.likes.extend(users_likes)
    if comments_id_exists(id, 'comments_has_dislikes'):
        users_dislikes = read_query('''SELECT U.name FROM users AS U
                                    INNER JOIN comments_has_dislikes AS CHD ON CHD.users_id = U.id
                                    WHERE CHD.comments_id = ?''', (id,))
        comment.dislikes.extend(users_dislikes)

    return comment


def create(comment: Comment):
    generated_id = insert_query(
        'INSERT INTO comments(body, create_at, topics_id, users_id) VALUES(?,?,?,?)',
        (comment.body, CURRENT_DATE_TIME, comment.topics_id, comment.users_id))

    comment.id = generated_id
    comment.create_at = CURRENT_DATE_TIME

    return comment


def exists(id: int):
    return any(
        read_query(
            'SELECT id, body FROM comments WHERE id = ?',
            (id,)))


def update(old: Comment, new: Comment):
    merged = Comment(
        id=old.id,
        body=new.body or old.body,
        create_at=old.create_at,
        update_at=CURRENT_DATE_TIME,
        topics_id=new.topics_id or old.topics_id,
        users_id=new.users_id,
        replies=new.replies or old.replies)

    update_query(
        '''UPDATE comments SET
           body = ?, create_at = ?, update_at = ?, topics_id = ?, users_id = ?
           WHERE id = ? 
        ''',
        (merged.body, merged.create_at, merged.update_at, merged.topics_id, merged.users_id, merged.id))

    return merged


def like(comment_id, user_id):
    insert_query('INSERT INTO comments_has_likes(comments_id, users_id) VALUES(?, ?) ', 
                 (comment_id, user_id))
    return f"Comment with id {comment_id} is liked"

def dislike(comment_id, user_id):
    insert_query('INSERT INTO comments_has_dislikes(comments_id, users_id) VALUES(?, ?) ', 
                 (comment_id, user_id))
    return f"Comment with id {comment_id} is disliked"

def remove_dislike(user_id):
    insert_query('DELETE FROM comments_has_dislikes WHERE users_id = ?', 
                 (user_id,))

def remove_like(user_id):
    insert_query('DELETE FROM comments_has_likes WHERE users_id = ?', 
                 (user_id,))
        
def delete(comments_id):
    insert_query('DELETE FROM comments WHERE id = ?', 
                 (comments_id,))
    return {'message': f'Comment with id {comments_id} deleted.'}


def is_topic_locked(comment_id: int):
    topic_lock = read_query('''
     SELECT is_locked FROM topics WHERE id = ?''', (comment_id,))

    return bool(topic_lock[0][0]) if topic_lock else None
