
from fastapi import HTTPException, Response
from core.models.topic import Topics
from database.database_connection import read_query, insert_query, update_query, read_query_one
from core.models.category import Category 
from services.utility import CURRENT_DATE_TIME, name_exists, paginate


def all_cat(search: str = None):
    if search is None:
        categories = read_query('SELECT * FROM categories')
    else:
        categories = read_query('SELECT * FROM categories WHERE name LIKE ?', (f'%{search}%',))
    
    result = (Category.from_query_result(*cat) for cat in categories)

    return result if categories else HTTPException(status_code=404)


def all_categories_without_private(user_id:int, search: str = None):
    if search is None:
        categories = read_query('''
        SELECT * FROM categories 
        WHERE is_private != 1 OR id IN (SELECT categories_id FROM private_categories WHERE users_id = ?)''',(user_id,))
    else:
        categories = read_query('''
        SELECT * FROM categories 
        WHERE is_private != 1 AND name LIKE ?''', (f'%{search}%',))  

    return (Category.from_query_result(*cat) for cat in categories) if categories else HTTPException(status_code=404)
        

def sort_cat(categories: list[Category], *, attribute='created', reverse=False):
    if attribute == 'created':
        def sort_fn(c: Category): return c.created
    elif attribute == 'updated':
        def sort_fn(c: Category): return c.updated
    elif attribute == 'name':
        def sort_fn(c: Category): return c.name
    else:
        def sort_fn(c: Category): return c.id

    return sorted(categories, key=sort_fn, reverse=reverse)


def get_by_id(id: int, search: str=None):
    category = read_query_one('SELECT * FROM categories WHERE id = ?',
                              (id,))
    if category is None:
        raise HTTPException(status_code=404, detail=f'Category with id: {id} does not exist.')
    
    real_category = Category.from_query_result(*category)

    if search is None:
        category_topics = read_query('''SELECT * FROM topics where categories_id = ?''', (id,))
    else:
        category_topics = read_query('SELECT * FROM topics WHERE categories_id = ? AND title LIKE ?', (id, f'%{search}%'))    
    topics = [Topics.from_query_result(*x) for x in category_topics]
    real_category.topics = topics
    
    return real_category 


def create_cat(category: Category):
    category_id = insert_query(
                 'INSERT INTO categories(name, create_at, is_locked, is_private) VALUES (?, ?, ?, ?)',
                 (category.name, CURRENT_DATE_TIME, False, 0)
                 )
    category.id = category_id
    category.created = CURRENT_DATE_TIME
    return category


def update_cat(old_category: Category, new_category: Category):
    merged = Category(
        id=old_category.id,
        name=new_category.name or old_category.name,
        created=old_category.created,
        updated=CURRENT_DATE_TIME,
        is_locked=new_category.is_locked or old_category.is_locked
    )

    update_query(
        '''UPDATE categories SET
           name = ?, create_at = ?, update_at = ?, is_locked = ?
           WHERE id = ? 
        ''',
        (merged.name, merged.created, merged.updated,merged.is_locked, merged.id))

    return merged


def delete_cat(id: int):
    insert_query('''DELETE categories, topics
                    FROM categories
                    RIGHT JOIN topics ON topics.categories_id = categories.id
                    WHERE categories.id = ?''',
                 (id,))
    # insert_query('DELETE FROM categories WHERE id = ?',
    #              (id,))


def set_category_lock(id: int, lock: bool):
    update_query('''
                UPDATE categories SET is_locked = ? WHERE id = ?''', (lock, id))


def set_private(id: int, private: bool):
    update_query('''
                update categories set is_private = ? where id = ?''', (private, id))


def set_non_private(id: int, private: bool):
    update_query('''
                update categories set is private = ? where id = ?''', (private, id))


category_access = {
    "no_access": 0,
    "read_access": 1,
    "write_access": 2
}

access_type = {
    0:'no_access',
    1:'read_access',
    2:'write_access',
}

def read_access(category_id: int, user_id: int):
    insert_query('DELETE FROM private_categories WHERE users_id = ?',(user_id,))
    insert_query('''
                INSERT INTO private_categories(categories_id, users_id, access) VALUES(?,?,?)''',
                 (category_id, user_id, category_access["read_access"]))
    return {'message':f'Read access given to user {user_id} for category {category_id}'}


def write_access(category_id: int, user_id: int):
    insert_query('DELETE FROM private_categories WHERE users_id = ?',(user_id,))
    insert_query('''
                INSERT INTO private_categories(categories_id, users_id, access) VALUES(?,?,?)''',
                 (category_id, user_id, category_access["write_access"]))
    return {'message':f'Write access given to user {user_id} for category {category_id}'}

def revoke_access(category_id: int, user_id: int):
    insert_query('''
                DELETE FROM private_categories WHERE categories_id = ? AND users_id = ?''', (category_id, user_id))


def get_privileged_users(id: int):
    users = read_query('''
    select users.name, users.email, private_categories.access from users
join private_categories on private_categories.users_id = users.id where private_categories.categories_id = ?''', (id,))
    list_of_users = []
    for user in users:
        user_info = {}
        user_info.update({'name': user[0], 'email': user[1], 'access': access_type[user[2]]})
        list_of_users.append(user_info)
    return list_of_users


def set_private(id: int, private: bool):
    update_query('''
                update categories set is_private = ? where id = ?''', (private, id))


def set_non_private(id: int, private: bool):
    update_query('''
                update categories set is_private = ? where id = ?''', (private, id))


def check_category_for_access(category_id:int, users_id:int):
    access_num = read_query_one(
        'SELECT access FROM private_categories WHERE categories_id = ? AND users_id = ?',
        (category_id, users_id))
    admin = read_query_one('SELECT admin FROM users WHERE id = ?', 
                           (users_id,))
    if bool(admin[0]):
        return access_type[2]
    return access_type[access_num[0]] if access_num else None


def is_private(category_id: int):
    value = read_query_one('SELECT is_private FROM categories WHERE id=?', 
                      (category_id,))
    return bool(value[0]) 


def get_cat_id_from_forum_data(
        reply_id: int | None = None, 
        comment_id: int | None = None, 
        topic_id: int | None = None,
        ):

    query = ''

    if reply_id:
        query = '''
        SELECT categories.id FROM categories
        INNER JOIN topics ON topics.categories_id = categories.id
        INNER JOIN comments ON comments.topics_id = topics.id
        INNER JOIN replies ON replies.comment_id = comments.id
        WHERE replies.id = ?'''
    elif comment_id:
        query = '''
        SELECT categories.id FROM categories
        INNER JOIN topics ON topics.categories_id = categories.id
        INNER JOIN comments ON comments.topics_id = topics.id
        WHERE comments.id = ?''' 
    elif topic_id:
        query = '''
        SELECT categories.id FROM categories
        JOIN topics ON topics.categories_id = categories.id
        WHERE topics.id = ?''' 
   
    cat_id = read_query_one(query, (reply_id or comment_id or topic_id,)) 

    return cat_id[0] if cat_id else Response(status_code=404)





