import uvicorn
from fastapi import FastAPI
from routers.comments import comment_router
from routers.topics import topic_router
from routers.users import user_router
from routers.categories import categories_router
from routers.replies import replies_router
from routers.direct_messages import messages_router

app = FastAPI(
    title='Forum System API',
    version=0.1,
)

app.include_router(topic_router)
app.include_router(comment_router)
app.include_router(user_router)
app.include_router(categories_router)
app.include_router(replies_router)
app.include_router(messages_router)


if __name__ == '__main__':
    uvicorn.run('main:app', reload=True)



