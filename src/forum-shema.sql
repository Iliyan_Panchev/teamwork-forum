-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema forum_schema
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema forum_schema
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `forum_schema` DEFAULT CHARACTER SET latin1 ;
USE `forum_schema` ;

-- -----------------------------------------------------
-- Table `forum_schema`.`categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum_schema`.`categories` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `create_at` DATETIME NOT NULL,
  `update_at` DATETIME NULL DEFAULT NULL,
  `is_locked` TINYINT(4) NULL DEFAULT NULL,
  `is_private` TINYINT(4) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 26
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `forum_schema`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum_schema`.`users` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `email` VARCHAR(45) NOT NULL,
  `password` VARCHAR(999) NOT NULL,
  `create_at` DATETIME NOT NULL,
  `update_at` DATETIME NULL DEFAULT NULL,
  `admin` TINYINT(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `email_UNIQUE` (`email` ASC) VISIBLE)
ENGINE = InnoDB
AUTO_INCREMENT = 19
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `forum_schema`.`topics`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum_schema`.`topics` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `title` VARCHAR(150) NOT NULL,
  `body` VARCHAR(250) NOT NULL,
  `create_at` DATETIME NOT NULL,
  `update_at` DATETIME NULL DEFAULT NULL,
  `categories_id` INT(11) NOT NULL,
  `users_id` INT(11) NULL DEFAULT NULL,
  `best_comment_id` INT(11) NULL DEFAULT NULL,
  `is_locked` TINYINT(4) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_topics_categories_idx` (`categories_id` ASC) VISIBLE,
  INDEX `fk_topics_users1_idx` (`users_id` ASC) VISIBLE,
  INDEX `fk_topics_comments1_idx` (`best_comment_id` ASC) VISIBLE,
  CONSTRAINT `fk_topics_categories`
    FOREIGN KEY (`categories_id`)
    REFERENCES `forum_schema`.`categories` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_topics_comments1`
    FOREIGN KEY (`best_comment_id`)
    REFERENCES `forum_schema`.`comments` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_topics_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `forum_schema`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 21
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `forum_schema`.`comments`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum_schema`.`comments` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `body` VARCHAR(45) NOT NULL,
  `create_at` DATETIME NOT NULL,
  `update_at` DATETIME NULL DEFAULT NULL,
  `topics_id` INT(11) NULL DEFAULT NULL,
  `users_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_messages_topics1_idx` (`topics_id` ASC) VISIBLE,
  INDEX `fk_messages_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_messages_topics1`
    FOREIGN KEY (`topics_id`)
    REFERENCES `forum_schema`.`topics` (`id`)
    ON DELETE CASCADE,
  CONSTRAINT `fk_messages_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `forum_schema`.`users` (`id`)
    ON DELETE SET NULL
    ON UPDATE SET NULL)
ENGINE = InnoDB
AUTO_INCREMENT = 51
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `forum_schema`.`comments_has_dislikes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum_schema`.`comments_has_dislikes` (
  `users_id` INT(11) NULL DEFAULT NULL,
  `comments_id` INT(11) NULL DEFAULT NULL,
  INDEX `fk_users_has_comments_comments1_idx` (`comments_id` ASC) VISIBLE,
  INDEX `fk_users_has_comments_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_users_has_comments_comments1`
    FOREIGN KEY (`comments_id`)
    REFERENCES `forum_schema`.`comments` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_comments_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `forum_schema`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `forum_schema`.`comments_has_likes`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum_schema`.`comments_has_likes` (
  `comments_id` INT(11) NULL DEFAULT NULL,
  `users_id` INT(11) NULL DEFAULT NULL,
  INDEX `fk_comments_has_users_users1_idx` (`users_id` ASC) VISIBLE,
  INDEX `fk_comments_has_users_comments1_idx` (`comments_id` ASC) VISIBLE,
  CONSTRAINT `fk_comments_has_users_comments1`
    FOREIGN KEY (`comments_id`)
    REFERENCES `forum_schema`.`comments` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_comments_has_users_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `forum_schema`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `forum_schema`.`direct_messages`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum_schema`.`direct_messages` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `body` VARCHAR(45) NOT NULL,
  `sent_at` DATETIME NOT NULL,
  `sender_id` INT(11) NULL DEFAULT NULL,
  `receiver_id` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_direct_messages_users1_idx` (`sender_id` ASC) VISIBLE,
  INDEX `fk_direct_messages_users2_idx` (`receiver_id` ASC) VISIBLE,
  CONSTRAINT `fk_direct_messages_users1`
    FOREIGN KEY (`sender_id`)
    REFERENCES `forum_schema`.`users` (`id`)
    ON DELETE SET NULL
    ON UPDATE SET NULL,
  CONSTRAINT `fk_direct_messages_users2`
    FOREIGN KEY (`receiver_id`)
    REFERENCES `forum_schema`.`users` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 27
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `forum_schema`.`private_categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum_schema`.`private_categories` (
  `categories_id` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  `access` INT(11) NULL DEFAULT NULL,
  INDEX `fk_categories_has_users_users1_idx` (`users_id` ASC) VISIBLE,
  INDEX `fk_categories_has_users_categories1_idx` (`categories_id` ASC) VISIBLE,
  CONSTRAINT `fk_categories_has_users_categories1`
    FOREIGN KEY (`categories_id`)
    REFERENCES `forum_schema`.`categories` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_categories_has_users_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `forum_schema`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `forum_schema`.`replies`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `forum_schema`.`replies` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `body` VARCHAR(45) NOT NULL,
  `create_at` DATETIME NOT NULL,
  `update_at` DATETIME NULL DEFAULT NULL,
  `comment_id` INT(11) NOT NULL,
  `users_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_replies_messages1_idx` (`comment_id` ASC) VISIBLE,
  INDEX `fk_replies_users1_idx` (`users_id` ASC) VISIBLE,
  CONSTRAINT `fk_replies_messages1`
    FOREIGN KEY (`comment_id`)
    REFERENCES `forum_schema`.`comments` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_replies_users1`
    FOREIGN KEY (`users_id`)
    REFERENCES `forum_schema`.`users` (`id`)
    ON DELETE CASCADE
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 10
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
