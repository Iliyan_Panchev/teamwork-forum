import unittest
from unittest.mock import Mock, create_autospec, patch
from fastapi import HTTPException, Response
from src.core.models.category import Category
from src.database import database_connection
from src.services import category_service

def create_category(id, name, created, updated, is_locked, is_private):
    return Category(
             id=id,
             name=name,
             created=created,
             updated=updated,
             is_locked=is_locked,
             is_private=is_private,
             )

class CategoryServiceShould(unittest.TestCase):
    def test_all_returnsAllCategoriesPresent(self):
            with patch('src.services.category_service.read_query') as mock_db_read_query:
                # Arrange
                mock_db_read_query.return_value = [
                    (1, 'Test_Category', '2023-04-30 16:27:00', None, 0, 0),
                    (2, 'Test_Category2', '2023-04-29 16:29:00', None, 1, 0),
                    (3, 'Test_Category3', '2023-04-28 16:30:00', None, 1, 1),
                ]
                expected = [
                    create_category(1, 'Test_Category', '2023-04-30 16:27:00', None, 0, 0),
                    create_category(2, 'Test_Category2', '2023-04-29 16:29:00', None, 1, 0),
                    create_category(3, 'Test_Category3', '2023-04-28 16:30:00', None, 1, 1),
                ]
                # Act
                result = category_service.all_cat()
                # Assert
                self.assertEqual(expected, list(result))


    def test_all_cat_returnsCategoryByNameSearch(self):
            with patch('src.services.category_service.read_query') as mock_db_read_query:
                # Arrange
                mock_db_read_query.return_value = [
                    (2, 'Test_Category2', '2023-04-29 16:29:00', None, 1, 0),
                ]
                expected = [
                    create_category(2, 'Test_Category2', '2023-04-29 16:29:00', None, 1, 0)
                ]
                # Act
                result = category_service.all_cat(search='Test_Category2')
                # Assert
                self.assertEqual(expected, list(result))


    def test_all_cat_returnsNoneWhenNoCategoriesPresent(self):
            with patch('src.services.category_service.read_query') as mock_db_read_query:
                # Arrange
                mock_db_read_query.return_value = []
                # Act
                result = category_service.all_cat()
                # Assert
                self.assertIsInstance(result, HTTPException)

    def test_get_by_id_returnsWhenCategoriesPresent(self):
            with patch('src.services.category_service.read_query_one') as mock_db_read_query_one:
                # Arrange
                mock_db_read_query_one.return_value = (3, 'Test_Category2', '2023-04-29 16:29:00', None, 1, 0)
                
                expected = create_category(3, 'Test_Category2', '2023-04-29 16:29:00', None, 1, 0)
                
                # Act
                result = category_service.get_by_id(3)
                # Assert
                self.assertEqual(expected, result)


    def test_get_by_id_raisesErrorWhenCategoriesNotPresent(self):
            with self.assertRaises(HTTPException):
                _ = category_service.get_by_id(1337)
           

