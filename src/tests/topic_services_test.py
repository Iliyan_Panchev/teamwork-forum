import unittest
from unittest.mock import patch
from fastapi import HTTPException
from src.core.models.topic import Topics
from src.services import topic_service


def create_topic(id, title, body, create_at, update_at , categories_id, users_id, best_comment_id, is_locked):
    return Topics(
        id=id,
        title=title,
        body=body,
        create_at=create_at,
        update_at=update_at,
        categories_id=categories_id,
        users_id=users_id,
        best_comment_id=best_comment_id,
        is_locked=is_locked
    )


class TopicServiceShould(unittest.TestCase):
    def test_return_all_topics(self):
        with patch('services.topic_service.read_query') as mock_db_read_query:
            # Arrange
            mock_db_read_query.return_value = [
                 (1, 'Test_Topic1', 'Just Test', '2023-04-30 16:27:00', None, 1, 1, None, 0),
                 (2, 'Test_Topic2', 'Just Test', '2023-04-29 16:29:00', None, 1, 2, None, 1),
                 (3, 'Test_Topic3', 'Just Test', '2023-04-28 16:30:00', None, 1, 3, None, 1)]
            expected = [
                create_topic(1, 'Test_Topic1', 'Just Test', '2023-04-30 16:27:00', None, 1, 1, None, 0),
                create_topic(2, 'Test_Topic2', 'Just Test', '2023-04-29 16:29:00', None, 1, 2, None, 1),
                create_topic(3, 'Test_Topic3', 'Just Test', '2023-04-28 16:30:00', None, 1, 3, None, 1)
            ]
            # Act
            result = topic_service.all_topics()
            # Assert
            self.assertEqual(expected, list(result))

    def test_return_topics_search_by_name(self):
        with patch('services.topic_service.read_query') as mock_db_read_query:
            # Arrange
            mock_db_read_query.return_value = [
                 (1, 'Test_Topic1', 'Just Test', '2023-04-30 16:27:00', None, 1, 1, None, 0),
                 ]
            expected = [
                create_topic(1, 'Test_Topic1', 'Just Test', '2023-04-30 16:27:00', None, 1, 1, None, 0)
            ]
            # Act
            result = topic_service.all_topics(search='Test_Topic1')
            # Assert
            self.assertEqual(expected, list(result))

    def test_return_empty_list_when_no_topics(self):
        with patch('services.topic_service.read_query') as mock_db_read_query:
            # Arrange
            mock_db_read_query.return_value = []
            expected = []
            # Act
            result = topic_service.all_topics()
            # Assert
            self.assertEqual(expected, list(result))

    def test_Create_topic(self):
        with patch('services.topic_service.insert_query') as mock_db_insert_query:
            test_id = 2
            mock_db_insert_query.return_value = {'message':f'Topic with id {test_id} deleted.'}
            expected = {'message':f'Topic with id {test_id} deleted.'}
            result = topic_service.delete(test_id)
            self.assertEqual(expected, result)

    def test_set_topic_lock(self):
        with patch('services.topic_service.update') as mock_db_update_query:
            test_id = 2
            mock_db_update_query.return_value = "Topic is locked"
            expected = "Topic is locked"
            result = topic_service.set_topic_lock(test_id, True)
            self.assertEqual(expected, result)

    def test_set_topic_unlock(self):
        with patch('services.topic_service.update_query') as mock_db_update_query:
            test_id = 2
            mock_db_update_query.return_value = "Topic is unlocked"
            expected = "Topic is unlocked"
            result = topic_service.set_topic_lock(test_id, False)
            self.assertEqual(expected, result)


    def test_return_404_where_topic_not_found(self):
        with self.assertRaises(HTTPException):
            _ = topic_service.get_by_id(1337)

