import unittest
from unittest.mock import Mock
from fastapi.testclient import TestClient
from src.main import app
from src.core.models.user import UserLogin
from src.routers.users import login
import src.routers.topics as topic_router

mock_category_service = Mock(spec='src.services.category_service')
mock_topic_service = Mock(spec='src.services.topic_service')
topic_router.topic_service = mock_topic_service
topic_router.category_service = mock_category_service

admin_login = UserLogin(
    email='userx@example.com', 
    password='string')

user_login = UserLogin(
    email='user@example.com', 
    password='string')

admin_token = login(admin_login)['access token']
user_token = login(user_login)['access token']

test_topic = {
    'title': 'Test Title Whatever',
    'body': 'Test Body Whatever',
    'categories_id': 22,
    'users_id': 22
}

class TopicRouterShould(unittest.TestCase):
    def setUp(self) -> None:
        self.client = TestClient(app)
        mock_category_service.reset_mock()
        mock_topic_service.reset_mock()


    def test_create_topic_returnsWhenUserLogged(self):
        topic_router.topic_service.is_category_locked = lambda id: False
        topic_router.category_service.check_category_for_access = lambda cat_id, user_id: 'write_access'
        topic_router.category_service.is_private = lambda cat_id: False
        topic_router.topic_service.title_exists = lambda title, table: False
        topic_router.topic_service.create = lambda topic: True

        response = self.client.post('/topics/', json=test_topic, headers={'Authorization': f'Bearer {user_token}'})
        self.assertEqual(response.status_code, 201)


    def test_create_topic_returnsWhenCategoryIsLocked(self):
        topic_router.topic_service.is_category_locked = lambda id: True

        response = self.client.post('/topics/', json=test_topic, headers={'Authorization': f'Bearer {user_token}'})
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {'detail': 'This topic is part of a locked Category'})


    def test_create_topic_returnsWhenUserWrongAccess(self):
        topic_router.topic_service.is_category_locked = lambda id: False
        topic_router.category_service.check_category_for_access = lambda cat_id, user_id: 'read_access'
        topic_router.category_service.is_private = lambda cat_id: True

        response = self.client.post('/topics/', json=test_topic, headers={'Authorization': f'Bearer {user_token}'})


        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {'detail': 'You need write access to create this private category reply.'})


    def test_create_topic_returnsWhenTitleExists(self):
        topic_router.topic_service.is_category_locked = lambda id: False
        topic_router.category_service.check_category_for_access = lambda cat_id, user_id: 'write_access'
        topic_router.category_service.is_private = lambda cat_id: False
        topic_router.topic_service.title_exists = lambda title, table: True
  

        response = self.client.post('/topics/', json=test_topic, headers={'Authorization': f'Bearer {user_token}'})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {'detail': 'Topic with name Test Title Whatever already exist!'})


