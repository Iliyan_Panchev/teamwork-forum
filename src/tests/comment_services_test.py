import unittest
from unittest.mock import patch
from fastapi import HTTPException
from src.core.models.comment import Comment
from src.services import comment_service


def create_comment(id, body, create_at, update_at, topics_id, users_id):
    return Comment(id=id,
                   body=body,
                   create_at=create_at,
                   update_at=update_at,
                   topics_id=topics_id,
                   users_id=users_id
                   )


class CommentServiceShould(unittest.TestCase):
    def test_return_all_comments(self):
        with patch('services.comment_service.read_query') as mock_db_read_query:
            # Arrange
            mock_db_read_query.return_value = [
                (1, 'Test_comment', '2023-04-30 16:27:00', None, 1, 1),
                (2, 'Test_comment2', '2023-04-30 16:27:00', None, 1, 1),
                (3, 'Test_comment3', '2023-04-30 16:27:00', None, 1, 1)]
            expected = [
                create_comment(1, 'Test_comment', '2023-04-30 16:27:00', None, 1, 1),
                create_comment(2, 'Test_comment2', '2023-04-30 16:27:00', None, 1, 1),
                create_comment(3, 'Test_comment3', '2023-04-30 16:27:00', None, 1, 1)
            ]
            # Act
            result = comment_service.all_comments()
            # Assert
            self.assertEqual(expected, list(result))

    def test_return_empty_list_when_no_comments(self):
        with patch('services.comment_service.read_query') as mock_db_read_query:
            # Arrange
            mock_db_read_query.return_value = []
            expected = []
            # Act
            result = comment_service.all_comments()
            # Assert
            self.assertEqual(expected, list(result))

    def test_delete_comment(self):
        with patch('services.comment_service.insert_query') as mock_db_insert_query:
            test_id = 2
            mock_db_insert_query.return_value = {'message': f'Comment with id {test_id} deleted.'}
            expected = {'message': f'Comment with id {test_id} deleted.'}
            result = comment_service.delete(test_id)
            self.assertEqual(expected, result)

    def test_like_comment(self):
        with patch('services.comment_service.insert_query') as mock_db_insert_query:
            test_id = 2
            test_user_id = 1
            mock_db_insert_query.return_value = f"Comment with id {test_id} is liked"
            expected = f"Comment with id {test_id} is liked"
            result = comment_service.like(test_id, test_user_id)
            self.assertEqual(expected, result)

    def test_dislike_comment(self):
        with patch('services.comment_service.insert_query') as mock_db_insert_query:
            test_id = 2
            test_user_id = 1
            mock_db_insert_query.return_value = f"Comment with id {test_id} is disliked"
            expected = f"Comment with id {test_id} is disliked"
            result = comment_service.dislike(test_id, test_user_id)
            self.assertEqual(expected, result)

    def test_return_404_when_comment_not_found(self):
        with self.assertRaises(HTTPException):
            _ = comment_service.get_by_id(1337)
