import unittest
from unittest.mock import Mock
from fastapi.testclient import TestClient
from src.core.models.category import Category
from src.main import app
from src.core.models.user import UserLogin
from src.routers.users import login
import src.routers.categories as category_router

mock_category_service = Mock(spec='src.services.category_service')
mock_topic_service = Mock(spec='src.services.topic_service')
category_router.topic_service = mock_topic_service
category_router.category_service = mock_category_service

admin_login = UserLogin(email='userx@example.com', password='string')
user_login = UserLogin(email='user@example.com', password='string')

admin_token = login(admin_login)['access token']
user_token = login(user_login)['access token']

test_id = 5

category_data = {
            'name': 'Test Category'
        }

test_category = Category(name=category_data['name'])

class CategoryRouterShould(unittest.TestCase):

    def setUp(self):
        self.client = TestClient(app)
        mock_category_service.reset_mock()
        mock_topic_service.reset_mock()


    def test_create_cat_returnsWhenAdminRightsReturns(self):
        category_router.category_service.name_exists = lambda id, location: False
        category_router.category_service.create_cat = lambda x: True
        

        response = self.client.post('/categories/', json=category_data, headers={'Authorization': f'Bearer {admin_token}'})
        self.assertEqual(response.status_code, 201)


    def test_create_cat_returnsWhenNotAdminRightsReturns(self):
        category_router.topic_service.is_admin = lambda token: False
        
        response = self.client.post('/categories/', json=category_data, headers={'Authorization': f'Bearer {user_token}'})
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {'detail': 'Only admins can create categories.'})


    def test_create_cat_returnsWhenDuplicateName(self):
        category_router.category_service.name_exists = lambda id, location: True
      
        response = self.client.post('/categories/', json=category_data, headers={'Authorization': f'Bearer {admin_token}'})
        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.json(), {'detail': 'Category with name: Test Category already exist!'})


    def test_delete_cat_returnsWhenCategoryExists(self):
        category_router.id_exists = lambda id, location: True
        category_router.category_service.delete_cat = lambda cat: True
        
        response = self.client.delete(f'/categories/{test_id}', headers={'Authorization': f'Bearer {admin_token}'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'message':f'Category with id: {test_id} deleted.'})


    def test_delete_cat_returnsWhenCategoryDoesntExist(self):
        category_router.id_exists = lambda id, location: False
        category_router.category_service.delete_cat = lambda cat: True
        

        response = self.client.delete(f'/categories/{test_id}', headers={'Authorization': f'Bearer {admin_token}'})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.json(), {'detail': f'Category with id: {test_id} does not exist'})


    def test_delete_cat_returnsWhenCategoryNotAdmin(self):
        category_router.id_exists = lambda id, location: True
        category_router.category_service.delete_cat = lambda cat: True
        
        response = self.client.delete(f'/categories/{test_id}', headers={'Authorization': f'Bearer {user_token}'})
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {'detail': 'Only admins can delete categories.'})


    def test_lock_category_returnsWhenCategoryExists(self):
        category_router.id_exists = lambda id, location: True
        category_router.category_service.set_category_lock = lambda cat, bool: True
        category_router.category_service.get_by_id = lambda id: test_category

        response = self.client.patch(f'/categories/{test_id}/lock_category', headers={'Authorization': f'Bearer {admin_token}'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'message': f"Category with id {test_id} is locked"})


    def test_lock_category_raisesWhenNotAdmin(self):
        category_router.id_exists = lambda id, location: True
        category_router.category_service.set_category_lock = lambda cat, bool: True
        category_router.category_service.get_by_id = lambda id: test_category

        response = self.client.patch(f'/categories/{test_id}/lock_category', headers={'Authorization': f'Bearer {user_token}'})
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {'detail': 'You must be admin to lock this category'})


    def test_unlock_category_returnsWhenCategoryExists(self):
        category_router.id_exists = lambda id, location: True
        category_router.category_service.set_category_lock = lambda cat, bool: True
        category_router.category_service.get_by_id = lambda id: test_category

        response = self.client.patch(f'/categories/{test_id}/unlock_category', headers={'Authorization': f'Bearer {admin_token}'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'message': f"Category with id {test_id} is unlocked"})


    def test_unlock_category_returnsWhenNotAdmin(self):
        category_router.id_exists = lambda id, location: True
        category_router.category_service.set_category_lock = lambda cat, bool: True
        category_router.category_service.get_by_id = lambda id: test_category

        response = self.client.patch(f'/categories/{test_id}/unlock_category', headers={'Authorization': f'Bearer {user_token}'})
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {'detail': 'You must be admin to unlock this category'})


    def test_unlock_category_returnsWhenNotAuthenticated(self):
        category_router.id_exists = lambda id, location: True
        category_router.category_service.set_category_lock = lambda cat, bool: True
        category_router.category_service.get_by_id = lambda id: test_category

        response = self.client.patch(f'/categories/{test_id}/unlock_category', headers={'Authorization': f'Bearer {user_token[1:1]}'})
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {'detail': 'Not authenticated'})


    def test_unlock_category_returnsWhenInvalidToken(self):
        category_router.id_exists = lambda id, location: True
        category_router.category_service.set_category_lock = lambda cat, bool: True
        category_router.category_service.get_by_id = lambda id: test_category

        response = self.client.patch(f'/categories/{test_id}/unlock_category', headers={'Authorization': f'Bearer {user_token[-1]}'})
        self.assertEqual(response.status_code, 403)
        self.assertEqual(response.json(), {'detail': 'Invalid or expired token'})